import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ms_fix/model/service_model.dart';
import 'package:ms_fix/screens/login_screen.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:carousel_slider/carousel_slider.dart';

final List<String> imgList = [
  'assets/images/cliente.png',
  'assets/images/prestador.png'
];

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
        model: UserModel(),
        child: ScopedModelDescendant<UserModel>(
          builder: (context, child, model){
            return ScopedModel<ServiceModel>(
              model: ServiceModel(model),
              child:MaterialApp(
                title: "Ms.Fix",
                theme: ThemeData(
                    primarySwatch: Colors.pink,
                    primaryColor: Color.fromARGB(255, 187, 55, 125)

                ),
                debugShowCheckedModeBanner: false,
                home:SplashScreen(
                  seconds: 3,
                  navigateAfterSeconds: new AfterSplash(),
                  image: new Image.asset('assets/images/iconmsfix.png'),
                  backgroundColor: Colors.white,
                  photoSize: 100.0,
                  loaderColor: Color.fromARGB(255, 187, 55, 125),
                ),
              ),
            );
          },
        )
    );
  }
}


class AfterSplash extends StatefulWidget {
  State<StatefulWidget> createState() {
    return _AfterSplash();
  }
}

class _AfterSplash extends State<AfterSplash> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(

      body: Column(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                  viewportFraction: 1.0,
                  height: height-30,
                  enlargeCenterPage: false,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }
              ),
              items: imgList.map((item) => Container(
                child: GestureDetector(
                  child: Image.asset(item, fit: BoxFit.cover),
                  onTap: (){
                    if(_current == 0){
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context)=> LoginScreen("tomador"))
                      );
                    }else{
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context)=> LoginScreen("prestador"))
                      );
                    }
                  }
                ),
              )).toList(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: imgList.map((url) {
                int index = imgList.indexOf(url);
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index
                        ? Color.fromRGBO(0, 0, 0, 0.9)
                        : Color.fromRGBO(0, 0, 0, 0.4),
                  ),
                );
              }).toList(),
            ),
          ]
      ),
    );
  }
}
