import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'dart:io';
typedef VoidCallback = void Function();

class OrcamentoModel extends Model {
  FirebaseAuth _auth = FirebaseAuth.instance;
  AuthResult authResult;
  FirebaseUser firebaseUser;
  Map<String, dynamic> orcamento = Map();

  bool isLoading = false;

  static OrcamentoModel of(BuildContext context) =>
      ScopedModel.of<OrcamentoModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
  }

  void cadastroOrcamento(
      {@required Map<String, dynamic> orcamento,
      //@required String codCliente,
      @required String descricao,
      @required String material,
      @required String valorServico,
      @required VoidCallback onSuccess,
      @required VoidCallback onFail}) {

    isLoading = true;
    notifyListeners();

    Firestore.instance.collection("orcamento").document().setData(orcamento);

      notifyListeners();

      onSuccess();
      isLoading = false;
      notifyListeners();
    }
  }
