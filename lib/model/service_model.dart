import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'dart:io';


class ServiceModel extends Model{

  UserModel user;

  ServiceModel(this.user){
    if(user.isLoggedIn())
      print("logado");
  }

  bool isLoading = false;

  static ServiceModel of(BuildContext context) => ScopedModel.of<ServiceModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);
  }
}