import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:async';
import 'dart:io';

class UserModel extends Model {

  FirebaseAuth _auth = FirebaseAuth.instance;
  AuthResult authResult;
  FirebaseUser firebaseUser;
  Map<String, dynamic> userData = Map();
  Map<String, dynamic> addressData = Map();
  String tipousuario;
  String tipoUser;
  String codUser;

  bool isLoading = false;

  static UserModel of(BuildContext context) =>
      ScopedModel.of<UserModel>(context);

  @override
  void addListener(VoidCallback listener) {
    super.addListener(listener);

    _loadCurrentUser(tipousuario);
  }

  void signUp({@required Map<String,
      dynamic> userData, @required String pass, @required VoidCallback onSuccess, @required VoidCallback onFail, @required File imgFile, @required Map<String,
      dynamic> addressData, @required String tipousuario}) {
    isLoading = true;
    notifyListeners();

    _auth.createUserWithEmailAndPassword(
        email: userData["email"], password: pass).then((authResult) async {
      firebaseUser = authResult.user;

      StorageUploadTask task = FirebaseStorage.instance.ref().child("users/RG/"+firebaseUser.uid).putFile(imgFile);

      StorageTaskSnapshot taskSnapshot = await task.onComplete;
      String url = await taskSnapshot.ref.getDownloadURL();
      userData["RG"] = url;

      await Firestore.instance.collection("user-data").document("users").collection(tipousuario).document(firebaseUser.uid).setData(userData);
      await Firestore.instance.collection("user-data").document("users").collection(tipousuario).document(firebaseUser.uid).collection("location").document("home_address").setData(addressData);
      notifyListeners();

      onSuccess(
      );
      isLoading = false;
      notifyListeners();

    }).catchError((e){
      onFail();
      isLoading = false;
      notifyListeners();
    });

  }

  void signIn({@required String email, @required String pass, @required VoidCallback onSuccess,@required VoidCallback onFail, @required String tipousuario}) async{

    isLoading = true;
    notifyListeners();

    _auth.signInWithEmailAndPassword(email: email, password: pass).then((authResult) async{
      firebaseUser = authResult.user;
      await _loadCurrentUser(tipousuario);

      onSuccess();
      isLoading = false;
      notifyListeners();

    }).catchError((e){
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  void signOut() async {
    await _auth.signOut();

    userData = Map();
    firebaseUser = null;

    notifyListeners();
  }

  bool isLoggedIn(){
    return firebaseUser != null;
  }

  Future<Null> _loadCurrentUser(tipousuario) async {
    if(firebaseUser == null)
      firebaseUser = await _auth.currentUser();
    if(firebaseUser != null){
      DocumentSnapshot docUser =
      await Firestore.instance.collection("user-data").document("users").collection(tipousuario).document(firebaseUser.uid).get();
      this.userData = docUser.data;
      this.userData['name'] = docUser['name'];
      this.userData['fotoperfil'] = docUser['fotoperfil'];

      tipoUser = tipousuario;
    };
  }
  void recoverPass(String email){
    _auth.sendPasswordResetEmail(email: email);
  }

}