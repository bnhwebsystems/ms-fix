import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ms_fix/screens/orcamentos_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/orcamento_model.dart';
import 'package:ms_fix/screens/home_screen.dart';

class CadastroOrcamentoScreen extends StatefulWidget {

  @override
  _CadastroOrcamentoScreenState createState() => _CadastroOrcamentoScreenState();
}

class _CadastroOrcamentoScreenState extends State<CadastroOrcamentoScreen> {
  final _descricaoController = TextEditingController();
  final _materialController = TextEditingController();
  final _valorServicoController = TextEditingController();
  //final _codClienteController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Cadastro de Orçamento", style: TextStyle(
            fontFamily: "OpenSans",
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          )),
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(255, 187, 55, 125),
                    Color.fromARGB(255, 251, 211, 233)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                )),
          ),
        ),
        body: ScopedModelDescendant<OrcamentoModel>(
          builder: (context, child, model) {
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );

            return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(16.0),
                children: <Widget>[
                  TextFormField(
                    controller: _descricaoController,
                    decoration: InputDecoration(hintText: "Descrição do serviço"),
                    validator: (text) {
                      if (text.isEmpty) return "Preencha o campo";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _materialController,
                    decoration: InputDecoration(hintText: "Material necessário"),
                    validator: (text) {
                      if (text.isEmpty) return "Preencha o campo";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _valorServicoController,
                    decoration: InputDecoration(hintText: "Valor final"),
                    obscureText: true,
                    validator: (text) {
                      if (text.isEmpty) return "Preencha o campo";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    height: 44.0,
                    child: RaisedButton(
                      child: Text(
                        "Cadastrar orçamento",
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                      textColor: Colors.white,
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Map<String, dynamic> orcamento = {
                            "descricao": _descricaoController.text,
                            "material": _materialController.text,
                            "valorServico": _valorServicoController.text,
                          };

                          model.cadastroOrcamento(
                              orcamento: orcamento,
                              descricao: _descricaoController.text,
                              material: _materialController.text,
                              valorServico: _valorServicoController.text,
                              );
                        }
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }

  void _onSuccess() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Orçamento cadastrado com sucesso!"),
      backgroundColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 2),
    ));
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => OrcamentosScreen()));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao cadastrar orcamento!"),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 2),
    ));
  }
}
