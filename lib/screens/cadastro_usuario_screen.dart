import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ms_fix/screens/login_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/screens/home_screen.dart';

class SignUpScreen extends StatefulWidget {
  final String tipousuario;
  const SignUpScreen(this.tipousuario);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _phoneController = TextEditingController();
  final _birthdayController = TextEditingController();
  final _cpfController = TextEditingController();
  final _ufController = TextEditingController();
  final _cityController = TextEditingController();
  final _streetController = TextEditingController();
  final _housenumberController = TextEditingController();
  String _opcao;

  File imgFile;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var currentSelectedValue;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("CADASTRAR-SE", style: TextStyle(
            fontFamily: "OpenSans",
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          )),
          centerTitle: true,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromARGB(255, 187, 55, 125),
                    Color.fromARGB(255, 251, 211, 233)
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                )),
          ),
        ),
        body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );

            return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(16.0),
                children: <Widget>[
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(hintText: "Nome Completo"),
                    validator: (text) {
                      if (text.isEmpty) return "Nome Inválido!";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(hintText: "E-mail"),
                    keyboardType: TextInputType.emailAddress,
                    validator: (text) {
                      if (text.isEmpty || !text.contains("@"))
                        return "E-mail inválido!";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _passController,
                    decoration: InputDecoration(hintText: "Senha"),
                    obscureText: true,
                    validator: (text) {
                      if (text.isEmpty || text.length < 6)
                        return "Senha inválida!";
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _phoneController,
                    decoration: InputDecoration(hintText: "Número de celular"),

                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    children: <Widget>[
                      Text("Clique aqui para upload do RG"),
                      IconButton(
                        icon: Icon(Icons.account_box),
                        onPressed: () async {
                          final FilePickerResult result = await FilePicker
                              .platform
                              .pickFiles(type: FileType.image);
                          File file = File(result.files.first.path);
                          imgFile = file;
                        },
                      ),

                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _birthdayController,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      DataInputFormatter(),
                    ],
                    decoration: InputDecoration(hintText: "Data de nascimento"),

                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _cpfController,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                      CpfInputFormatter(),
                    ],
                    decoration: InputDecoration(hintText: "CPF"),

                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _streetController,
                    decoration: InputDecoration(hintText: "Rua / Avenida"),

                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  TextFormField(
                    controller: _housenumberController,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    decoration: InputDecoration(hintText: "Nº da casa"),

                  ),
                  SizedBox(
                    height: 16.0,

                  ),
                  TextFormField(
                    controller: _cityController,
                    decoration: InputDecoration(hintText: "Cidade"),

                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  DropdownButton(
                    hint: Text('Escolha seu Estado'),
                    value: currentSelectedValue,
                    items: Estados.listaEstadosSigla.map((String _opcao){
                      return DropdownMenuItem(
                        value: _opcao,
                        child: Text(_opcao),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      setState(() {
                        currentSelectedValue = newValue;
                      });
                      print(currentSelectedValue);
                    },
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  SizedBox(
                    height: 44.0,
                    child: RaisedButton(
                      child: Text(
                        "Criar Conta",
                        style: TextStyle(
                          fontSize: 18.0,
                        ),
                      ),
                      textColor: Colors.white,
                      color: Theme.of(context).primaryColor,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Map<String, dynamic> userData = {
                            "name": _nameController.text,
                            "email": _emailController.text,
                            "phone": _phoneController.text,
                            "birthday": _birthdayController.text,
                          };

                          Map<String, dynamic> addressData= {
                            "UF" : _opcao.toUpperCase(),
                            "city": _cityController.text.toUpperCase(),
                            "street": _streetController.text.toUpperCase(),
                            "house number": _housenumberController.text,
                          };

                          model.signUp(
                              userData: userData,
                              pass: _passController.text,
                              onSuccess: _onSuccess,
                              onFail: _onFail,
                              imgFile: imgFile,
                              addressData: addressData,
                              tipousuario: widget.tipousuario);
                        }







                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }

  void _onSuccess() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Usuário criado com sucesso!"),
      backgroundColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 2),
    ));
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => LoginScreen(widget.tipousuario)));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao criar usuário!"),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 2),
    ));
  }
}
