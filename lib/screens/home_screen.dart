import 'package:flutter/material.dart';
import 'package:ms_fix/screens/cadastro_usuario_screen.dart';
import 'package:ms_fix/widgets/custom_drawer.dart';
import 'package:ms_fix/tabs/home_tab.dart';
import 'package:ms_fix/screens/visitas_screen.dart';
import 'package:ms_fix/screens/orcamentos_screen.dart';
import 'package:ms_fix/screens/servicos_screen.dart';

class HomeScreen extends StatelessWidget {
  final _pageController = PageController();
  final String titulo = null;

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[

        Scaffold(
          appBar: AppBar(
            title: Text("Home"),
            centerTitle: true,
            flexibleSpace: Container(

              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 187, 55, 125),
                      Color.fromARGB(255, 251, 211, 233)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: HomeTab(_pageController),

        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Criar Conta"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 187, 55, 125),
                      Color.fromARGB(255, 251, 211, 233)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: Container(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Visitas"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 187, 55, 125),
                      Color.fromARGB(255, 251, 211, 233)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: VisitasScreen(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Orçamentos"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 187, 55, 125),
                      Color.fromARGB(255, 251, 211, 233)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: OrcamentosScreen(),
        ),
        Scaffold(
          appBar: AppBar(
            title: Text("Serviços"),
            centerTitle: true,
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(255, 187, 55, 125),
                      Color.fromARGB(255, 251, 211, 233)
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  )),
            ),
          ),
          drawer: CustomDrawer(_pageController),
          body: ServicosScreen(),
        ),
        Container(color: Colors.pink),
      ],
    );
  }
}
