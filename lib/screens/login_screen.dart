import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/screens/cadastro_usuario_screen.dart';
import 'package:ms_fix/screens/home_screen.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginScreen extends StatefulWidget {
  final String tipousuario;
  const LoginScreen(this.tipousuario);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailController = TextEditingController();
  final _passController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: ScopedModelDescendant<UserModel>(
        builder: (context, child, model) {
          if (model.isLoading)
            return Center(
              child: CircularProgressIndicator(),
            );

          return  SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                            AssetImage('assets/images/Background-login.png'),
                            fit: BoxFit.fill)),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 5),
                          height: MediaQuery.of(context).size.height*0.30,
                          child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                height: MediaQuery.of(context).size.height*0.06,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage('assets/images/iconmsfix.png'),
                                      fit: BoxFit.fitHeight
                                  ),
                                ),
                              )),
                        ),
                        Padding(
                          padding: EdgeInsets.all(30.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          color:
                                          Color.fromARGB(255, 251, 211, 233),
                                          blurRadius: 20.0,
                                          offset: Offset(0, 10))
                                    ]),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[400]))),
                                      child: TextFormField(
                                        controller: _emailController,
                                        keyboardType: TextInputType.emailAddress,
                                        validator: (text) {
                                          if (text.isEmpty || !text.contains("@"))
                                            return "E-mail inválido!";
                                        },
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "Insira seu e-mail",
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(8.0),
                                      decoration: BoxDecoration(
                                          border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.grey[400]))),
                                      child: TextFormField(
                                        controller: _passController,
                                        obscureText: true,
                                        validator: (text){
                                          if(text.isEmpty || text.length < 6) return "Senha inválida!";
                                        },
                                        decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: "Insira sua senha",
                                            hintStyle: TextStyle(
                                                color: Colors.grey[400])),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              SizedBox(
                                height: 30.0,
                                width: 450.0,

                                child: RaisedButton(
                                  child: Text("Entrar",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                    ),
                                  ),

                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  textColor: Colors.white,
                                  color:     Color.fromARGB(255, 187, 55, 125) ,
                                  onPressed: (){
                                    if(_formKey.currentState.validate()){

                                    }
                                    model.signIn(
                                        email: _emailController.text,
                                        pass: _passController.text,
                                        onSuccess: _onSuccess,
                                        onFail: _onFail,
                                        tipousuario: widget.tipousuario
                                    );
                                  },
                                ),
                              ),
                              SizedBox(height: 16.0,),
                              SizedBox(
                                height: 30.0,
                                width: 450.0,
                                child: RaisedButton(
                                  child: Text("Cadastrar-se",
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      color: Colors.white,
                                      fontFamily: "OpenSans",
                                    ),
                                  ),

                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  textColor: Colors.white,
                                  color:     Color.fromARGB(255, 251, 211, 233) ,
                                  onPressed: (){
                                    Navigator.of(context).push(
                                        MaterialPageRoute(builder: (context)=> SignUpScreen(widget.tipousuario))
                                    );
                                  },
                                ),
                              ),
                              SizedBox(
                                height:5,
                              ),
                              Container (
                                height:MediaQuery.of(context).size.height* 0.10,
                                decoration: BoxDecoration(
                                    color: Colors.transparent
                                ),
                                child: Center(
                                  child: Text("Esqueci minha senha",
                                      style: TextStyle(
                                          color:  Color.fromARGB(255, 187, 55, 125),
                                          fontWeight: FontWeight.w300)),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _onSuccess(){
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text("Entrada realizada com sucesso"),
          backgroundColor: Colors.blue,
          duration: Duration(seconds: 2),
        )
    );
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context)=>HomeScreen()));
  }

  void _onFail(){
    _scaffoldKey.currentState.showSnackBar(
        SnackBar(content: Text("Falha ao Entrar!"),
          backgroundColor: Colors.redAccent,
          duration: Duration(seconds: 2),
        )
    );
  }

}
