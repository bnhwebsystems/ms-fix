import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/screens/home_screen.dart';
import 'dart:math';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/main.dart';

class MatchScreen extends StatefulWidget {
  final String cod;
  final String codUser;
  const MatchScreen(this.cod, this.codUser);
  @override
  _MatchScreenState createState() => _MatchScreenState();
}

class _MatchScreenState extends State<MatchScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  
  @override
  Widget build(BuildContext context) {
    var snapshots = Firestore.instance
        .collection('user-data').document('users').collection('prestador')
        .snapshots();

    var nomePrestador;
    int i = 0;

    return Scaffold(
      appBar: AppBar(
        title: Text("Procurando prestadora..."),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 187, 55, 125),
                  Color.fromARGB(255, 251, 211, 233)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
      body: StreamBuilder(
        stream: snapshots,
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot,
            ) {
          if(snapshot.hasError){
            return Center(child: Text('ERRO: ${snapshot.error} '));
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data.documents.length == 0){
            return Center(child: Text('Nenhum orçamento ainda :('));
          }
          if(snapshot.hasData){
            i = Random().nextInt(snapshot.data.documents.length);
            //Pega o id do prestador:
            DocumentSnapshot snap = snapshot.data.documents[i];
            //Pega o nome do prestador:
            nomePrestador = snapshot.data.documents[i].data;
            var item = snapshot.data.documents[i].data;
            var doc = snapshot.data.documents[i];
            var contratada = item['contratada'];
            contratada = false;

            if(contratada == false)
              return Center(
              child: ListTile(
                leading: IconButton(
                    icon: Icon(Icons.check, color: Colors.green),
                    onPressed: () async => {
                      modalAviso(context),
                      await Firestore.instance.collection('visita').add({
                        'codCliente':  widget.codUser ,
                        'codPrestador': doc.documentID ,
                        'tipoServico': widget.cod ,
                        'contratada' : true,
                      }),

                    },
                ),
                isThreeLine: true,
                title: Text(item['name']),
                subtitle: Text("Especializada em: ${widget.cod}"),
                trailing: IconButton(
                  icon: Icon(Icons.delete_forever, color: Colors.red),
                  onPressed: () => null,
                ),
              ),
            );
          }

          return Text("tem nada");
        },
      ),
    );
  }

  modalAviso(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () => {
        Navigator.of(context).pop(),
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context)=> HomeScreen())
        ),
      }
    );
    AlertDialog alerta = AlertDialog(
      title: Text("Você acaba de contratar uma visita!"),
      content: Text("   Avisaremos a prestadora de serviço e logo você receberá a ajuda que precisa! \n   Você será redirecionada para a página inicial do aplicativo :)"),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }
}