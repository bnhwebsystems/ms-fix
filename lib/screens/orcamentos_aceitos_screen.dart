import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'orcamentos_cadastrados_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/main.dart';

class OrcamentosAceitosScreen extends StatefulWidget {
  final String codUser;
  const OrcamentosAceitosScreen(this.codUser);
  @override
  _OrcamentosAceitosScreenState createState() => _OrcamentosAceitosScreenState();
}

class _OrcamentosAceitosScreenState extends State<OrcamentosAceitosScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var snapshots = Firestore.instance
        .collection('orcamento')
        .snapshots();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Serviços Solicitados"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 187, 55, 125),
                  Color.fromARGB(255, 251, 211, 233)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
      body: StreamBuilder(
        stream: snapshots,
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot,
            ) {
          if(snapshot.hasError){
            return Center(child: Text('ERRO: ${snapshot.error} '));
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data.documents.length == 0){
            return Center(child: Text('Nenhum orçamento ainda :('));
          }
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext context, int i){
              var item = snapshot.data.documents[i].data;
              var doc = snapshot.data.documents[i];
              var codPrestador = item['codPrestador'];
              var selectedUser = item['codCliente'];
              var descricao = item['descricao'];
              var material = item['material'];
              var tipoServico = item['tipoServico'];
              var valor = item['valorServico'];
              log(selectedUser);
              if((widget.codUser == codPrestador) && (item['excluido'] == false) && (item['contratado'] == true)){
                return Container(
                  color: Colors.white,
                  margin: const EdgeInsets.fromLTRB(10, 3, 10, 3 ),
                  child: ListTile(
                    leading: IconButton(
                        icon: Icon(Icons.check, color: Colors.green),
                        onPressed: ()=> {
                          modalCreateServico(context, widget.codUser, selectedUser, descricao, material, tipoServico, valor)
                        },
                    ),
                    isThreeLine: true,
                    title: Text("${item['tipoServico']} - R\$ ${item['valorServico']}"),
                    subtitle: Text("${item['descricao']}"),
                    trailing: IconButton(
                      icon: Icon(Icons.delete_forever, color: Colors.red),
                      onPressed: () => null ,
                    ),
                  ),
                );
                // ignore: missing_return
              }
              return Container(width: 0, height: 0,); ;
            },
          );
        },
      ),
    );
  }

  modalCreateServico(BuildContext context, String codUser, String codCliente, String descricao, String material, String tipoServico, String valor){
    var form = GlobalKey<FormState>();
    var nomeTomador;

    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Aceitar serviço?') ,
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Cancelar'),
            ),
            FlatButton(
              color: Color.fromARGB(255, 187, 55, 125) ,
              onPressed: () async {

                  if(codCliente != null){
                    await Firestore.instance.collection('servico').add({
                      'tipoServico': tipoServico,
                      'descricao' : descricao,
                      'material' : material,
                      'valorServico' : valor,
                      'codCliente' : codCliente,
                      'codPrestador' : codUser,
                    });

                    Navigator.of(context).pop();
                  }

              },
              child: Text('Sim'),
            ),
          ],
        );
      },
    );
  }
}