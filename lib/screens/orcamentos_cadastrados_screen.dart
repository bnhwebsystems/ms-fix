import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class OrcamentosCadastradosScreen extends StatefulWidget {
  final String codUser;
  const OrcamentosCadastradosScreen(this.codUser);

  @override
  _OrcamentosCadastradosScreenState createState() => _OrcamentosCadastradosScreenState();
}

class _OrcamentosCadastradosScreenState extends State<OrcamentosCadastradosScreen> {
  @override
  Widget build(BuildContext context) {
    var snapshots = Firestore.instance
        .collection('orcamento')
        .snapshots();

    return Scaffold(
      appBar: AppBar(
        title: Text("Consulta de Orçamentos"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 187, 55, 125),
                  Color.fromARGB(255, 251, 211, 233)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
      body: StreamBuilder(
        stream: snapshots,
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot,
            ) {
          if(snapshot.hasError){
            return Center(child: Text('ERRO: ${snapshot.error} '));
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data.documents.length == 0){
            return Center(child: Text('Nenhum orçamento ainda :('));
          }
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext context, int i){
              var item = snapshot.data.documents[i].data;
              var doc = snapshot.data.documents[i];
              var codCliente = item['codCliente'];
              if((widget.codUser == codCliente) && (item['excluido'] == false) && (item['contratado'] == false)){
                return Container(
                  color: Colors.white,
                  margin: const EdgeInsets.fromLTRB(10, 3, 10, 3 ),
                  child: ListTile(
                    leading: IconButton(
                      icon: Icon(Icons.check, color: Colors.green),
                      onPressed: ()=> {
                        modalAviso(context),
                        doc.reference.updateData({
                          'contratado': true,
                        })
                      }
                    ),
                    isThreeLine: true,
                    title: Text("${item['tipoServico']} - R\$ ${item['valorServico']}"),
                    subtitle: Text(item['descricao']),
                    trailing: IconButton(
                      icon: Icon(Icons.delete_forever, color: Colors.red),
                      onPressed: () => doc.reference.updateData({
                        'excluido': true,
                      }) ,
                    ),
                  ),
                );
              // ignore: missing_return
              }
              return Container( width: 0.0, height: 0.0);
            },
          );
        },
      ),
    );
  }

  modalAviso(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () => Navigator.of(context).pop(),
    );
    AlertDialog alerta = AlertDialog(
      title: Text("Agradecemos a resposta!"),
      content: Text("Sua resposta foi enviada a prestadora de serviços! Caso ela aceite realizar o serviço, a resposta dela aparecerá para você no menu 'Serviços'."),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }

}