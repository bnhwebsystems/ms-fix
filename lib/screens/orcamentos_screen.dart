import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'orcamentos_cadastrados_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/main.dart';

class OrcamentosScreen extends StatefulWidget {
  @override
  _OrcamentosScreenState createState() => _OrcamentosScreenState();
}

class _OrcamentosScreenState extends State<OrcamentosScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var currentSelectedValue;
  String tipoUsuario;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            var codUser = model.firebaseUser.uid;
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else
              if(model.tipoUser == "tomador")
                return Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                  children: <Widget>[
                    Text("Consulte seus orçamentos!",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                    Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                      child: Text("Ao clicar no botão abaixo, poderá ver os orçamentos que foram feitos especialmente para você! Poderá também escolher realizar ou não o serviço! ",
                          style: TextStyle(height: 1.5, fontSize: 16)),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 20),
                      child: Text("*Lembre-se: uma vez que rejeita o orçamento, ele será excluído e não poderá mais vê-lo",
                          style: TextStyle(fontSize: 14, height: 1.5)),
                    ),
                    SizedBox(height: 16.0),
                    SizedBox(
                      height: 44.0,
                      child: RaisedButton(
                        child: Text(
                          "Consultar Orçamentos",
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                        textColor: Colors.white,
                        color: Color.fromARGB(255, 187, 55, 125),
                        onPressed: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context)=> OrcamentosCadastradosScreen(codUser))
                          );
                        }
                    ),
                  ),
                  ],
                ),
              );
              else if(model.tipoUser == "prestador")
                return Form(
                  key: _formKey,
                  child: ListView(
                    padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                    children: <Widget>[
                      Text("Faça um orçamento!",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                        child: Text("Ao cadastrar um orçamento, ele se torna disponivel para que seu cliente possa visualizar e decidir se vai contratá-lo para a realização do serviço!",
                            style: TextStyle(height: 1.5, fontSize: 16)),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(16, 0, 16, 20),
                        child: Text("*Lembre-se de preencher corretamente todos os campos de cadastro.",
                            style: TextStyle(fontSize: 14, height: 1.5)),
                      ),
                      SizedBox(
                        height: 44.0,
                        child: RaisedButton(
                          child: Text(
                            "Gerar Orçamento",
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          textColor: Colors.white,
                          color: Color.fromARGB(255, 187, 55, 125),
                          onPressed: () => modalCreate(context, codUser),
                        ),
                      ),
                    ],
                  ),
                );
              else
                return Form(
                  key: _formKey,
                  child: ListView(
                    padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                        child: Text("Faça login novamente para poder visualizar essa página! :)",
                            style: TextStyle(height: 1.5, fontSize: 16)),
                      ),
                      SizedBox(height: 16.0),
                      SizedBox(
                    height: 44.0,
                    child: RaisedButton(
                        child: Text(
                          "Fazer Login",
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                        textColor: Colors.white,
                        color: Color.fromARGB(255, 187, 55, 125),
                        onPressed: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(builder: (context)=> MyApp())
                          );
                        }
                    ),
                  ),
                    ],
                  ),
                );
          },
        ));
  }

  modalCreate(BuildContext context, String codUser){
    var form = GlobalKey<FormState>();
    var material = TextEditingController();
    var valorServico = TextEditingController();
    var tipoServico = TextEditingController();
    var descricao = TextEditingController();
    var nomeTomador;
    var selectedUser;

    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Novo Orçamento') ,
          content: Form(
            key: form,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Tipo de Serviço',
                    ),
                    controller: tipoServico,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Este campo não pode ser vazio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Descrição',
                    ),
                    controller: descricao,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Este campo não pode ser vazio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Material necessário',
                    ),
                    controller: material,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Este campo não pode ser vazio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Valor do Serviço',
                    ),
                    controller: valorServico,
                    validator: (value){
                      if(value.isEmpty){
                        return 'Este campo não pode ser vazio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(height: 16.0),
                  StreamBuilder<QuerySnapshot>(
                      stream: Firestore.instance.collection("user-data").document('users').collection('tomador').snapshots(),
                      builder: (context, snapshot){
                        if(!snapshot.hasData){
                          const Text("Carregando...");
                        }
                        else{
                          List<DropdownMenuItem> userItems = [] ;
                          for(int i=0 ; i<snapshot.data.documents.length; i++){
                            //Pega o id do Tomador:
                            DocumentSnapshot snap = snapshot.data.documents[i];

                            //Pega o nome do Tomador:
                            nomeTomador = snapshot.data.documents[i].data;
                            userItems.add(
                                DropdownMenuItem(
                                  child: Text(
                                    nomeTomador['name'],
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  value: "${snap.documentID}",
                                )
                            );
                          }
                          return Row(
                            children: <Widget>[
                              SizedBox(width: 50.0),
                              DropdownButton(
                                items: userItems,
                                onChanged: (userValue){
                                  setState(() {
                                    selectedUser = userValue;
                                  });
                                  print(selectedUser);
                                },
                                value: selectedUser,
                                isExpanded: false,
                                hint: new Text(
                                  "Selecione o cliente",
                                ),
                              ),
                            ],
                          );
                        }
                        return Container( width: 0.0, height: 0.0);
                      }
                  ),
                ],
              ),
            ),

          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Cancelar'),
            ),
            FlatButton(
              color: Color.fromARGB(255, 187, 55, 125) ,
              onPressed: () async {
                if(form.currentState.validate()){
                  if(selectedUser != null){
                    await Firestore.instance.collection('orcamento').add({
                      'tipoServico': tipoServico.text,
                      'descricao' : descricao.text,
                      'material' : material.text,
                      'valorServico' : valorServico.text,
                      'codCliente' : selectedUser,
                      'excluido' : false,
                      'contratado' : false,
                      'codPrestador' : codUser
                    });

                    Navigator.of(context).pop();
                  }
                  else{
                    return 'Este campo não pode ser vazio';
                  }
                }
              },
              child: Text('Salvar'),
            ),
          ],
        );
      },
    );
  }



}