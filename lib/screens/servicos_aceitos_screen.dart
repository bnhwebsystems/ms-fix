import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'orcamentos_cadastrados_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/main.dart';

class ServicosAceitosScreen extends StatefulWidget {
  final String codUser;
  const ServicosAceitosScreen(this.codUser);
  @override
  _ServicosAceitosScreenState createState() => _ServicosAceitosScreenState();
}

class _ServicosAceitosScreenState extends State<ServicosAceitosScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var snapshots = Firestore.instance
        .collection('servico')
        .snapshots();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Serviços Solicitados"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 187, 55, 125),
                  Color.fromARGB(255, 251, 211, 233)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
      body: StreamBuilder(
        stream: snapshots,
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot,
            ) {
          if(snapshot.hasError){
            return Center(child: Text('ERRO: ${snapshot.error} '));
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data.documents.length == 0){
            return Center(child: Text('Nenhum serviço ainda :('));
          }
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext context, int i){
              var item = snapshot.data.documents[i].data;
              var doc = snapshot.data.documents[i];
              var codCliente = item['codCliente'];
              if((widget.codUser == codCliente)){
                return Container(
                  color: Colors.white,
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 3 ),
                  padding: EdgeInsets.all(10),
                  child: ListBody(
                    children: [
                      Text(
                        "Tipo de serviço: ${item['tipoServico']} - R\$ ${item['valorServico']}",
                        textAlign: TextAlign.center,
                      ),
                      Container(margin: const EdgeInsets.fromLTRB(20, 10, 20, 10 )),
                      Text(
                          "Descrição:",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                      Container(margin: const EdgeInsets.fromLTRB(5, 3, 5, 3 )),
                      Text("${item['descricao']}"),
                      Container(margin: const EdgeInsets.fromLTRB(20, 10, 20, 10 )),
                      Text(
                          "Material: ",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                      Container(margin: const EdgeInsets.fromLTRB(5, 3, 5, 3 )),
                      Text("${item['material']}")
                    ],
                  ),
                );
                // ignore: missing_return
              }
              return Container(width: 0, height: 0,); ;
            },
          );
        },
      ),
    );
  }
}