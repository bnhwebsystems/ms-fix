import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'match_screen.dart';

class ServicosListaScreen extends StatefulWidget{
  final String codUser;
  const ServicosListaScreen(this.codUser);
  @override
  _ServicosListaScreenState createState() => _ServicosListaScreenState();
}

class _ServicosListaScreenState extends State<ServicosListaScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Solicitar visita"),
      ),
      body: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          Text("Qual o tipo de serviço você precisa que seja feito? ",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
          Padding(
            padding: EdgeInsets.only(top: 15, bottom: 30, left: 15, right: 15),
            child: Text("Escolha a baixo uma categoria, assim encontraremos uma colaboradora que pode te ajudar!",
                style: TextStyle(height: 1.5, fontSize: 16)),
          ),
          ListTile(
            leading: Icon(Icons.flash_on, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Elétrica'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Elétrica', widget.codUser ))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.water_damage_outlined, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Hidraúlica'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Hidraúlica', widget.codUser))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.add_moderator, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Dedetização'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Dedetização', widget.codUser))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.auto_fix_normal, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Montagem'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Montagem', widget.codUser))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.app_registration, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Instalação'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Instalação', widget.codUser))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.auto_awesome, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Acabamentos'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Acabamentos', widget.codUser))
              );
            },
          ),
          ListTile(
            leading: Icon(Icons.ad_units, color: Color.fromARGB(255, 187, 55, 125)),
            title: Text('Manutenção de eletrodomésticos'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: (){
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context)=> MatchScreen('Manutenção de eletrodomésticos', widget.codUser))
              );
            },
          ),
        ],
      ),
    );
  }

   modalSorter(BuildContext context){
    var form = GlobalKey<FormState>();
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Prestadora de serviço') ,
          content: Column(
                children: <Widget>[
                  StreamBuilder<QuerySnapshot>(
                      stream: Firestore.instance.collection("user-data").document('users').collection('prestador').snapshots(),
                      builder: (context, snapshot){
                        if(!snapshot.hasData){
                          const Text("Carregando...");
                        }
                        return ListView.builder(
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (BuildContext context, int i){
                            var item = snapshot.data.documents[i].data;
                              return Container(
                                color: Colors.white,
                                margin: const EdgeInsets.fromLTRB(10, 3, 10, 3 ),
                                child: ListTile(
                                  leading: IconButton(
                                      icon: Icon(Icons.check, color: Colors.green),
                                      onPressed: ()=> null,
                                  ),
                                  isThreeLine: true,
                                  title: Text("${item['name']} - R\$ ${item['phone']}"),
                                  subtitle: Text(item['email']),
                                  trailing: IconButton(
                                    icon: Icon(Icons.delete_forever, color: Colors.red),
                                    onPressed: () => null ,
                                  ),
                                ),
                              );
                          },
                        );
                      }
                  ),
                ],
              ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Cancelar'),
            ),
            FlatButton(
              color: Color.fromARGB(255, 187, 55, 125) ,
              onPressed: () async {
                if(form.currentState.validate()){
                  return 'TÁ TOP PATRÃO';
                }
              },
              child: Text('Aceitar'),
            ),
          ],
        );
      },
    );
  }
}