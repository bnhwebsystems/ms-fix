import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'orcamentos_cadastrados_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/main.dart';
import 'servicos_aceitos_screen.dart';
import 'orcamentos_aceitos_screen.dart';

class ServicosScreen extends StatefulWidget {
  @override
  _ServicosScreenState createState() => _ServicosScreenState();
}

class _ServicosScreenState extends State<ServicosScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            var codUser = model.firebaseUser.uid;
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            else
            if(model.tipoUser == "tomador")
              return Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                  children: <Widget>[
                    Text("Consulte seus serviços!",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                    Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                      child: Text("Ao clicar no botão abaixo, verá os serviços que foram aceitos pelas prestadoras de serviço! ",
                          style: TextStyle(height: 1.5, fontSize: 16)),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 20),
                      child: Text("*Lembre-se: uma vez que ela aceitou o serviço, basta esperar ela entrar em contato com você!",
                          style: TextStyle(fontSize: 14, height: 1.5)),
                    ),
                    SizedBox(height: 16.0),
                    SizedBox(
                      height: 44.0,
                      child: RaisedButton(
                          child: Text(
                            "Serviços Solicitados",
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          textColor: Colors.white,
                          color: Color.fromARGB(255, 187, 55, 125),
                          onPressed: () => {
                            Navigator.of(context).push(
                              MaterialPageRoute(builder: (context)=> ServicosAceitosScreen(codUser))
                            )
                          },
                      ),
                    ),
                  ],
                ),
              );
            else if(model.tipoUser == "prestador")
              return Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                  children: <Widget>[
                    Text("Veja os serviços que foram solicitados!",
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                    Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                      child: Text("Ao clicar no botão abaixo você verá os orçamentos que as clientes contrataram! Você pode decidir se conseguirá realizar o serviço ou não.",
                          style: TextStyle(height: 1.5, fontSize: 16)),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(16, 0, 16, 20),
                      child: Text("*Lembre-se de responder.",
                          style: TextStyle(fontSize: 14, height: 1.5)),
                    ),
                    SizedBox(
                      height: 44.0,
                      child: RaisedButton(
                        child: Text(
                          "Serviços Solicitados",
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                        textColor: Colors.white,
                        color: Color.fromARGB(255, 187, 55, 125),
                        onPressed: () => {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context)=> OrcamentosAceitosScreen(codUser))
                          ),
                        },
                      ),
                    ),
                  ],
                ),
              );
            else
              return Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                      child: Text("Faça login novamente para poder visualizar essa página! :)",
                          style: TextStyle(height: 1.5, fontSize: 16)),
                    ),
                    SizedBox(height: 16.0),
                    SizedBox(
                      height: 44.0,
                      child: RaisedButton(
                          child: Text(
                            "Fazer Login",
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          textColor: Colors.white,
                          color: Color.fromARGB(255, 187, 55, 125),
                          onPressed: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (context)=> MyApp())
                            );
                          }
                      ),
                    ),
                  ],
                ),
              );
          },
        ));
  }
}