import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ServicosSolicitadosScreen extends StatefulWidget {
  final String user;
  const ServicosSolicitadosScreen(this.user);

  @override
  _ServicosSolicitadosScreenState createState() => _ServicosSolicitadosScreenState();
}

class _ServicosSolicitadosScreenState extends State<ServicosSolicitadosScreen> {
  @override
  Widget build(BuildContext context) {
    var snapshots = Firestore.instance
        .collection('visita')
        .snapshots();

    return Scaffold(
      appBar: AppBar(
        title: Text("Consulta de Visitas"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color.fromARGB(255, 187, 55, 125),
                  Color.fromARGB(255, 251, 211, 233)
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
      body: StreamBuilder(
        stream: snapshots,
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot,
            ) {
          if(snapshot.hasError){
            return Center(child: Text('ERRO: ${snapshot.error} '));
          }
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data.documents.length == 0){
            return Center(child: Text('Nenhum orçamento ainda :('));
          }
          return ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (BuildContext context, int i){
              var item = snapshot.data.documents[i].data;

              var codPrestador = item['codPrestador'];

              if( (widget.user == codPrestador) && (item['contratada'] == true)){
                return Container(
                  color: Colors.white,
                  margin: const EdgeInsets.fromLTRB(10, 3, 10, 3 ),
                  child: ListTile(
                    leading: IconButton(
                        icon: Icon(Icons.check, color: Colors.green)
                    ),
                    isThreeLine: false,
                    title: Text(item['tipoServico']),
                    trailing: IconButton(
                      icon: Icon(Icons.delete_forever, color: Colors.red),
                    ),
                  ),
                );
              }
              else
                return Container( width: 0.0, height: 0.0);
            },
          );
        },
      ),
    );
  }

  modalAviso(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("Ok"),
      onPressed: () => Navigator.of(context).pop(),
    );
    AlertDialog alerta = AlertDialog(
      title: Text("Agradecemos a resposta!"),
      content: Text("Sua resposta foi enviada a prestadora de serviços! Caso ela aceite realizar o serviço, a resposta dela aparecerá para você no menu 'Serviços'."),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }

}