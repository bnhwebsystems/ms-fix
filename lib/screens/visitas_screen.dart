import 'package:flutter/material.dart';
import 'package:ms_fix/screens/servicos_lista_screen.dart';
import 'package:ms_fix/widgets/custom_drawer.dart';
import 'package:ms_fix/tabs/home_tab.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/main.dart';
import 'servicos_solicitados_screen.dart';
import 'home_screen.dart';

class VisitasScreen extends StatefulWidget {
  @override
  _VisitasScreenState createState() => _VisitasScreenState();
}

class _VisitasScreenState extends State<VisitasScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: ScopedModelDescendant<UserModel>(
          builder: (context, child, model) {
            var user = model.firebaseUser.uid;
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            if(model.tipoUser == "tomador")
              return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(20.0),
                children: <Widget>[
                  Text("Solicitação de visitas",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 30, left: 20, right: 20),
                    child: Text("Você pode solicitar uma visita para que uma de nossas colaboradoras vá até sua residência, analise o problema e te entregue um orçamento!",
                        style: TextStyle(height: 1.5, fontSize: 16)),
                  ),
                  SizedBox(
                    height: 44.0,
                    child: RaisedButton(
                      child: Text(
                        "Solicitar Visita",
                        style: TextStyle(fontSize: 18.0,
                        ),
                      ),
                      textColor: Colors.white,
                      color: Color.fromARGB(255, 187, 55, 125),
                      onPressed: (){
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context)=> ServicosListaScreen(user))
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
            else if(model.tipoUser == "prestador")
              return Form(
              key: _formKey,
              child: ListView(
                padding: EdgeInsets.all(20.0),
                children: <Widget>[
                  Text("Solicitações de visitas",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20), ),
                  Padding(
                    padding: EdgeInsets.only(top: 15, bottom: 30, left: 20, right: 20),
                    child: Text("Para visualizar as clientes que solicitaram sua visita, clique no botão abaixo.",
                        style: TextStyle(height: 1.5, fontSize: 16)),
                  ),
                  SizedBox(
                    height: 44.0,
                    child: RaisedButton(
                      child: Text(
                        "Solicitações de Visita",
                        style: TextStyle(fontSize: 18.0,
                        ),
                      ),
                      textColor: Colors.white,
                      color: Color.fromARGB(255, 187, 55, 125),
                      onPressed: () => {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (context)=> ServicosSolicitadosScreen(user))
                        ),
                      }
                    ),
                  ),
                ],
              ),
            );
            else
              return Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(16, 16, 16,0),
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 15, bottom: 15, left: 15, right: 15),
                      child: Text("Faça login novamente para poder visualizar essa página! :)",
                          style: TextStyle(height: 1.5, fontSize: 16)),
                    ),
                    SizedBox(height: 16.0),
                    SizedBox(
                      height: 44.0,
                      child: RaisedButton(
                          child: Text(
                            "Fazer Login",
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                          textColor: Colors.white,
                          color: Color.fromARGB(255, 187, 55, 125),
                          onPressed: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (context)=> MyApp())
                            );
                          }
                      ),
                    ),
                  ],
                ),
              );
          },
        ));
  }
}