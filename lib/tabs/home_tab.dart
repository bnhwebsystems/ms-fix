import 'package:flutter/material.dart';
import 'package:ms_fix/model/user_model.dart';
import 'package:ms_fix/screens/orcamentos_screen.dart';
import 'package:ms_fix/screens/servicos_lista_screen.dart';
import 'package:scoped_model/scoped_model.dart';


class HomeTab extends StatelessWidget {
  int page;

  final PageController pageController;

  HomeTab(this.pageController);

  Widget build(BuildContext context) {
    Widget _buildBodyBack() => Container(
      decoration: BoxDecoration(),
    );

    return Stack(
      children: [
        Container(
          child: ScopedModelDescendant<UserModel>(
              builder: (context, child, model){
                var user = model.firebaseUser.uid;
                return Container(
                  child: ListView(
                    children: [
                      GestureDetector(

                        onTap: (){
                          page = 2;
                          pageController.jumpToPage(page);
                        },
                        child: Image.asset(
                          "assets/images/solicitar-servico.png"
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          page = 3;
                          pageController.jumpToPage(page);
                        },
                        child: Image.asset(
                            "assets/images/orcamento.png"
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          page = 4;
                          pageController.jumpToPage(page);
                        },
                        child: Image.asset(
                            "assets/images/servicos.png"
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          model.signOut();
                        },
                        child: Image.asset(
                            "assets/images/sair.png"
                        ),
                      )
                    ],
                  ),
                );
              }
          ),


        )
      ],
    );

  }
}
